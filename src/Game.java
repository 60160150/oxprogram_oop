import java.util.Scanner;

public class Game {

	private Player O,X;
	private Board board;
	
	public Game() {
		X = new Player('X');
		O = new Player('O');
	}
	public void play() {
		while(true) {
			PlayOne();
		}
		
	}
	public void PlayOne() {
		board = new Board(X,O);
		showWelecome();
		while(true) {
			showTable();
			showTurn();
			input();
			if (board.isFinish()) {
				break;
			}
			board.switchPlayer();
		}
		showTable();
		showWinner();
		showStat();
		
	}
	private void showStat() {
		System.out.println(X.getName()+" W,D,L : "+X.getWin()+" "+X.getDraw()+" "+X.getLose());
		System.out.println(O.getName()+" W,D,L : "+O.getWin()+" "+O.getDraw()+" "+O.getLose());
	}
	private void showWinner() {
		Player player = board.getWinner();
		System.out.println(player.getName()+" win...");
	}
	private void showWelecome() {
		System.out.println("Welecome to OX Game");
	}
	
	private void showTable() {
		char[][]table = board.getTable();
		System.out.println(" 1 2 3");
		for(int i = 0 ; i <table.length;i++) {
			System.out.print((i+1));
			for(int j = 0 ; j <table[i].length;j++) {
				System.out.print(" "+table[i][j]);
			}System.out.println();
		}
	}
	private void showTurn() {
		Player player = board.getCurrentPlayer();
		System.out.println(player.getName()+" turn : ");
	}
	private void input() {
		Scanner kb = new Scanner(System.in);
		while(true) {
			try {
				System.out.print("Please input Row Col : ");
				String input = kb.nextLine();
				String[]str = input.split(" ");
				if (str.length!=2) {
					System.out.println("Please input Row Col[1-3] (ex: 1 2)");
					continue;
				}
				int row = Integer.parseInt(str[0])-1;
				int col = Integer.parseInt(str[1])-1;
				if (board.setTable(row,col)==false) {
					System.out.println("Table is not empty!!");
					continue;
				}
				break;
			}catch (Exception e) {
				System.out.println("Please input Row Col[1-3] (ex: 1 2)");
				continue;
			}
		}
	}
}
